# Wheel of Time Names
Return a random Wheel of Time character

> As the Wheel of Time turns, places wear many names. Men wear many names, many faces. Different faces, but always the same man. Yet no one knows the Great Pattern the Wheel weaves, or even the Pattern of an Age. We can only watch, and study, and hope.
> 
> Moraine

To print out a random Wheel of Time character
```
npx wot-names
```

## References
Names obtained from http://sites.ugcs.caltech.edu/~karlh/cgi-bin/wot.cgi

## Roadmap
- [ ] Add option to support returning the description
